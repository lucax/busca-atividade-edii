#include <stdlib.h>
#include <stddef.h>
#include "t_time.h"

t_time *t_time_init(short int h, short int m, short int s){
	t_time *t;
	
	if(h < 0 || h > 23) return NULL;
	if(m < 0 || m > 59) return NULL;
	if(s < 0 || s > 59) return NULL;
	
	t = (t_time*)malloc(sizeof(t_time));
	t->h = h;
	t->m = m;
	t->s = s;
	
	return t;
}

void t_time_free(t_time *t){
	free(t);
}


short int t_time_cmp(t_time *t1, t_time *t2){
	int tp1, tp2;

	tp1 = t1->h*3600 + t1->m*60 + t1->s;
	tp2 = t2->h*3600 + t2->m*60 + t2->s;

	if(tp1 > tp2)
		return 1;
	else if(tp1 < tp2)
		return -1;
	else
		return 0;
}

short int t_time_get_h(t_time *t){
	return t->h;
}

short int t_time_get_m(t_time *t){
	return t->m;
}

short int t_time_get_s(t_time *t){
	return t->s;
}

