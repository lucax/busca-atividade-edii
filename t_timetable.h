#ifndef __T_TIMETABLE__
#define __T_TIMETABLE__

#include "t_time.h"

typedef struct {
	t_time *key;
	char *value;
}t_timetable_item;

typedef struct{
	t_timetable_item *table;
	int size,n;
}t_timetable;

t_timetable *t_timetable_init(int tam);

void t_timetable_put(t_timetable *tt, t_time *key, char *value);

char *t_timetable_get(t_timetable *tt, t_time *key);

void t_timetable_print(t_timetable *tt);

void t_timetable_delete(t_timetable *tt, t_time *key);

void t_timetable_free(t_timetable *tt);

int t_timetable_size(t_timetable *tt);

char t_timetable_is_empty(t_timetable *tt);

char t_timetable_contains(t_timetable *tt, t_time key);

t_time *t_timetable_min(t_timetable *tt);

t_time *t_timetable_max(t_timetable *tt);

t_time *t_timetable_floor(t_timetable *t, t_time *key);

int t_timetable_get_index(t_timetable *tt, t_time *key);

t_time *t_timetable_ceiling(t_timetable *tt, t_time *key);

int t_timetable_rank(t_timetable *tt, t_time *key);

void t_timetable_delete_min(t_timetable *tt);

void t_timetable_delete_max(t_timetable *tt);

int t_timetable_size_range(t_timetable *tt, t_time lo, t_time hi);

t_time *t_timetable_select(t_timetable *tt, int k);

t_time *t_timetable_keys(t_timetable *tt, t_time lo, t_time hi, int *p);

#endif
