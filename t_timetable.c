#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include "t_time.h"
#include "t_timetable.h"

t_timetable *t_timetable_init(int tam){
	t_timetable *tt;
	tt = (t_timetable*) malloc(sizeof(t_timetable));
	tt->table = (t_timetable_item*)malloc(sizeof(t_timetable_item)* tam);
	tt->size = tam;
	tt->n=0;

	return tt;
}

void t_timetable_put(t_timetable *tt, t_time *key, char *value){
	int i,j;

	if(tt->n == tt->size)
		exit(-1);

	i=0;
	while(i < tt->n && t_time_cmp(key, tt->table[i].key) > 0)
		i++;

	if(tt->n > i && t_time_cmp(key, tt->table[i].key) != 0){
		for(j=tt->n-1; j >= i; --j){
			tt->table[j+1].key = tt->table[j].key;
			tt->table[j+1].value = tt->table[j].value;
		}
	}
	tt->table[i].key = key;
	tt->table[i].value = value;
	if(tt->n == i)
		tt->n++;
}

void t_timetable_print(t_timetable *tt){
	int i;
	short int h,m,s;
	t_time *t;

	for(i=0; i<tt->n; i++){
		t = tt->table[i].key;
		h = t_time_get_h(t);
		m = t_time_get_m(t);
		s = t_time_get_s(t);

		printf("%02d - %02d:%02d:%02d => %s\n", i,h,m,s, tt->table[i].value);
	}
}

char *t_timetable_get(t_timetable *tt, t_time *key){
	int lo,hi,m,x;

	lo=0;
	hi=tt->n-1;

	while(lo <= hi){
		m = (lo+hi)/2;

		x = t_time_cmp(tt->table[m].key, key);
		if(x ==0)
			return (tt->table[m].value);
		else if(x == 1)
			hi = m -1;
		else 
			lo = m +1;

	}
	return NULL;
}

int t_timetable_get_index(t_timetable *tt, t_time *key){
	int l=0, h, m,x;
	h=tt->n-1;
	while(l <= h){
		m = (l+h)/2;
		x = t_time_cmp(tt->table[m].key, key);
		if(x == 0)
			break;
		else if(x == 1)
			h = m-1;
		else
			l = m+1;
	}
	if(l > h)
		return -1;
	else
		return m;
}

void t_timetable_delete(t_timetable *tt, t_time *key){
	int l=0, h, m,x;
	h = tt->n-1;

	while(l <= h){
		m = (l+h)/2;
		x = t_time_cmp(tt->table[m].key, key);
		if(x == 0)
			break;
		else if(x == 1)
			h = m-1;
		else
			l = m+1;
	}
	if(t_time_cmp(tt->table[m].key, key) == 0){

		t_time_free(tt->table[m].key);

		for(; m<tt->n-1; m++){
			tt->table[m].key = tt->table[m+1].key;
			tt->table[m].value = tt->table[m+1].value;
		}
		tt->n--;
	}
}

void t_timetable_free(t_timetable *tt){
	for(int i = 0; i < tt->n; i++){
		t_time_free(tt->table[i].key);
	}
	free(tt->table);
	free(tt);
}

int t_timetable_size(t_timetable *tt){
	return tt->n;
}

char t_timetable_is_empty(t_timetable *tt){
	return (tt->n == 0);
}

char t_timetable_contains(t_timetable *tt, t_time key){
	return (t_timetable_get(tt, &key) != NULL);
}

t_time *t_timetable_min(t_timetable *tt){
	t_time *ta;
	if(t_timetable_is_empty(tt))
		return (NULL);
	else{
		ta= tt->table[0].key;
		return (ta);
	}
}

t_time *t_timetable_max(t_timetable *tt){
	t_time *ta;
	if(t_timetable_is_empty(tt))
		return NULL;
	else{
		ta=tt->table[tt->n].key;
		return ta;
	}
}

t_time *t_timetable_floor(t_timetable *tt, t_time *key){
	t_time *ta;
	int i;
	if(t_timetable_is_empty(tt) || tt->n<=1)
		return NULL;
	else{
		i = t_timetable_get_index(tt, key);
		if (i <= 0)
			return NULL;
		else{
			ta = tt->table[i-1].key;
		}
	}
	return ta;
}

t_time *t_timetable_ceiling(t_timetable *tt, t_time *key){
	t_time *ta;
	int i;
	if(t_timetable_is_empty(tt) || tt->n<=1)
		return NULL;
	else{
		i = t_timetable_get_index(tt, key);
		if(i == tt->n-1)
			return NULL;
		else
			ta = tt->table[i+1].key;
	}

	return ta;
}

int t_timetable_rank(t_timetable *tt, t_time *key){
	int j,i =0;
	i = t_timetable_get_index(tt, key);
	if(i < 0)
		return -1;
	for(j=0; j < i; j++){}

	return j;
}

void t_timetable_delete_min(t_timetable *tt){

	if(!(t_timetable_is_empty(tt))){
		t_time_free(tt->table[0].key);

		for(int i =0 ; i<tt->n-1; i++){
			tt->table[i].key = tt->table[i+1].key;
			tt->table[i].value = tt->table[i+1].value;
		}
		tt->n--;
	}
}

void t_timetable_delete_max(t_timetable *tt){
	if(!(t_timetable_is_empty(tt))){
		t_time_free(tt->table[tt->n-1].key);
		tt->n--;
	}
}

int t_timetable_size_range(t_timetable *tt, t_time lo, t_time hi){
	int i,j,count=0;
	if(t_timetable_is_empty(tt))
		return -1;

	i = t_timetable_get_index(tt, &lo);
	j = t_timetable_get_index(tt, &hi);
	if(i < 0 || j < 0 || i > j)
		return -1; 
	
	for(; i <= j; i++) count++;

	return count;
}

t_time *t_timetable_select(t_timetable *tt, int k){
	return tt->table[k].key;
}

t_time *t_timetable_keys(t_timetable *tt, t_time lo, t_time hi, int *p){
	t_time *ta;
	int i,j,n;

	n = t_timetable_size_range(tt,lo,hi);

	ta = (t_time*)malloc(sizeof(t_time) * n);
	if(ta == NULL)
		return NULL;
	
	i = t_timetable_get_index(tt, &lo);
	j = t_timetable_get_index(tt, &hi);
		
	for(int k =0; i <= j; i++, k++){
		ta[k].s = tt->table[i].key->s;
		ta[k].m = tt->table[i].key->m;
		ta[k].h = tt->table[i].key->h;
	}

	p = &n;

	return ta;
}
