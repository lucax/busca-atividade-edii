all: program

program: t_time.o t_timetable.o main.o
	gcc t_time.o t_timetable.o main.o -lm -o program

t_time.o: t_time.c t_time.h
	gcc -c t_time.c

t_timetable.o: t_timetable.c t_timetable.h t_time.h
	gcc -c t_timetable.c

main.o: main.c t_time.h t_timetable.h
	gcc -c main.c

clear:
	rm -f *.o program
