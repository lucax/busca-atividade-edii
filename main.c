#include <stdio.h>
#include <stdlib.h>
#include "t_time.h"
#include "t_timetable.h"

void main(){

	t_time aux_ta,aux_ta2;

	t_time *ta;
	t_timetable *tt;
	char *str;
	size_t len;

	int h,m,s,size,x;

	scanf("%d", &size);
	tt = t_timetable_init(size);

	printf("is empty: %d\n",t_timetable_is_empty(tt));

	scanf("%d:%d:%d", &h,&m,&s);
	while(h >= 0){
		getchar();

		ta = t_time_init(h,m,s);

		str = NULL;
		len = getline(&str, &len, stdin);
		str[len-1]='\0';

		t_timetable_put(tt, ta, str);

		//	printf("test\n");

		scanf("%d:%d:%d", &h,&m,&s);
	}

	scanf("%d:%d:%d", &h,&m,&s);
	while(h >= 0){
		ta = t_time_init(h,m,s);
		str = t_timetable_get(tt,ta);

		if(str)
			printf("%02d:%02d:%02d => %s\n", h,m,s,str);
		else
			printf("%02d:%02d:%02d => nao encontrado\n", h,m,s);
		scanf("%d:%d:%d", &h,&m,&s);
	}

	// inicio teste
	t_timetable_print(tt);

	printf("size: %d\n",t_timetable_size(tt));
	printf("is empty: %d\n",t_timetable_is_empty(tt));

	aux_ta.h = 9;
	aux_ta.m = 10;
	aux_ta.s = 11;
	aux_ta2.h = 9;
	aux_ta2.m = 25;
	aux_ta2.s = 52;
	printf("func contains: %d\n", t_timetable_contains(tt, aux_ta));


	ta = t_timetable_min(tt);
	h = t_time_get_h(ta);
	m = t_time_get_m(ta);
	s = t_time_get_s(ta);
	printf("%02d:%02d:%02d => min\n", h,m,s);

	ta = t_timetable_max(tt);
	h = t_time_get_h(ta);
	m = t_time_get_m(ta);
	s = t_time_get_s(ta);
	printf("%02d:%02d:%02d => max\n", h,m,s);

	ta = t_timetable_floor(tt, &aux_ta);
	h = t_time_get_h(ta);
	m = t_time_get_m(ta);
	s = t_time_get_s(ta);
	printf("%02d:%02d:%02d => floor\n", h,m,s);

	ta = t_timetable_ceiling(tt, &aux_ta);
	h = t_time_get_h(ta);
	m = t_time_get_m(ta);
	s = t_time_get_s(ta);
	printf("%02d:%02d:%02d => ceiling\n", h,m,s);

	x = t_timetable_rank(tt, &aux_ta);
	printf(" %d => rank \n",x);

	ta = t_timetable_select(tt, x);
	h = t_time_get_h(ta);
	m = t_time_get_m(ta);
	s = t_time_get_s(ta);
	printf("%02d:%02d:%02d => select\n", h,m,s);


	printf(" %d => size range \n",t_timetable_size_range(tt, aux_ta, aux_ta2));

	t_timetable_delete(tt, &aux_ta);
	t_timetable_delete_min(tt);
	t_timetable_delete_max(tt);

	ta = t_timetable_keys(tt, aux_ta, aux_ta2, &x);
	
	for(int i = 0; i < x; i++){
	//	h = t_time_get_h(ta);
	//	m = t_time_get_m(ta);
	//	s = t_time_get_s(ta);
	//	printf("%02d:%02d:%02d => select\n", h,m,s);


	}


	t_timetable_print(tt);
	//fim teste
	
	t_timetable_free(tt);
}
